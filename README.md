# SPECFEM1D

SPECFEM1D simulates seismic wave propagation in a one-dimensional heterogeneous medium. It is a small code that allows users to learn how a spectral-element program is written.

## Documentation


## Developers


## License

GNU GENERAL PUBLIC LICENSE

Version 3, 29 June 2007

Copyright © 2007 Free Software Foundation, Inc. http://fsf.org/

Everyone is permitted to copy and distribute verbatim copies of this

license document, but changing it is not allowed.

## Copyright

Main historical authors: Dimitri Komatitsch and Jeroen Tromp

CNRS, France and Princeton University, USA $\copyright$ October 2017

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation (see Appendix [cha:License]).

Please note that by contributing to this code, the developer understands and agrees that this project and contribution are public and fall under the open source license mentioned above.
